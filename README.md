api/:存储接口对象层（自己封装的接口）
scripts/：存储测试脚本层（unittest框架实现的测试类、测试方法）
data/:存储.json数据文件
report/:存储生成的html测试报告
common/:存储通用的工具方法
config.py:存储项目的配置信息（全局变量）
run_suite.py:组装测试用例，生成测试报告的代码